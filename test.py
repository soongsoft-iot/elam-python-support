import unittest
import sys
import logging
import logging.handlers

import elampdu


class PseudoSerial:

    def __init__(self):
        self.buf = bytearray()
        
    def reset_input_buffer(self):
        self.clear()
        
    def write(self, data):
        self.buf.extend(data)
        
    def read(self, lon):
        br = self.buf[0:lon]
        self.buf = self.buf[lon:]
        return br
        

class TestElamPDU(unittest.TestCase):
    
    # read requests
    
    def test_ElamReadCoilsRQ(self):
        ser.write(elampdu.ElamReadCoilsRQ(1, 10).to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 1)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 10)
        
    def test_ElamReadDiscreteInputsRQ(self):
        ser.write(elampdu.ElamReadDiscreteInputsRQ(1, 10).to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 2)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 10)
        
    def test_ElamReadHoldingRegistersRQ(self):
        ser.write(elampdu.ElamReadHoldingRegistersRQ(1, 10).to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 3)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 10)

    def test_ElamReadInputRegistersRQ(self):
        ser.write(elampdu.ElamReadInputRegistersRQ(1, 10).to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 4)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 10)
        

    # write requests
    
    def test_ElamWriteCoilRQ(self):
        ser.write(elampdu.ElamWriteCoilRQ(1, b'\xff\x00').to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 5)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 1)
        self.assertEqual(rq.data, b'\xff\x00')
        
    def test_ElamWriteHoldingRegisterRQ(self):
        ser.write(elampdu.ElamWriteHoldingRegisterRQ(1, b'\xff\x00').to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 6)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 1)
        self.assertEqual(rq.data, b'\xff\x00')
        
    def test_ElamWriteCoilsRQ(self):
        ser.write(elampdu.ElamWriteCoilsRQ(1, 2, b'\xff\x00\xff\x00').to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 15)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 2)
        self.assertEqual(rq.data, b'\xff\x00\xff\x00')
        
    def test_ElamWriteHoldingRegistersRQ(self):
        ser.write(elampdu.ElamWriteHoldingRegistersRQ(1, 2, b'\xff\x00\xff\x00').to_bytes(300))
        stid, rq = elampdu.ElamRQ.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rq.fc, 16)
        self.assertEqual(rq.st_address, 1)
        self.assertEqual(rq.quantity, 2)
        self.assertEqual(rq.data, b'\xff\x00\xff\x00')
        
        
    # read responses
    
    def test_ElamReadCoilsRS(self):
        ser.write(elampdu.ElamReadCoilsRS(b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 1)
        self.assertEqual(rs.data, b'\xff\x00')
        
    def test_ElamReadDiscreteInputsRS(self):
        ser.write(elampdu.ElamReadDiscreteInputsRS(b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 2)
        self.assertEqual(rs.data, b'\xff\x00')
        
    def test_ElamReadHoldingRegistersRS(self):
        ser.write(elampdu.ElamReadHoldingRegistersRS(b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 3)
        self.assertEqual(rs.data, b'\xff\x00')
        
    def test_ElamReadInputRegistersRS(self):
        ser.write(elampdu.ElamReadInputRegistersRS(b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 4)
        self.assertEqual(rs.data, b'\xff\x00')


    # write responses
    
    def test_ElamWriteCoilRS(self):
        ser.write(elampdu.ElamWriteCoilRS(1, b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 5)
        self.assertEqual(rs.st_address, 1)
        self.assertEqual(rs.value, b'\xff\x00')

    def test_ElamWriteHoldingRegisterRS(self):
        ser.write(elampdu.ElamWriteHoldingRegisterRS(1, b'\xff\x00').to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 6)
        self.assertEqual(rs.st_address, 1)
        self.assertEqual(rs.value, b'\xff\x00')

    def test_ElamWriteCoilsRS(self):
        ser.write(elampdu.ElamWriteCoilsRS(1, 2).to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 15)
        self.assertEqual(rs.st_address, 1)
        self.assertEqual(rs.value, 2)

    def test_ElamWriteHoldingRegistersRS(self):
        ser.write(elampdu.ElamWriteHoldingRegistersRS(1, 2).to_bytes(300))
        stid, rs = elampdu.ElamRS.from_serial(ser)
        self.assertEqual(stid, 300)
        self.assertEqual(rs.fc, 16)
        self.assertEqual(rs.st_address, 1)
        self.assertEqual(rs.value, 2)


loghandler = logging.StreamHandler(sys.stdout)
loghandler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(name)s - %(message)s'))
logging.getLogger('elampdu').setLevel(logging.INFO)
logging.getLogger('elampdu').addHandler(loghandler)

ser = PseudoSerial()

if __name__ == '__main__':
    unittest.main()

