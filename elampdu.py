"""
Author: Daniel Quintela <dq@soongsoft.com.ar>
Created: 2023-12-05
License: MIT, Copyright (c) 2023-2024 Daniel Quintela
Project: ELAM protocol python support
Repository: https://gitlab.com/soongsoft-iot/elam-python-support
"""

from typing import Union
import struct
import binascii
import logging


class ElamUtil:
    
    """ElamUtil own LOGGER."""
    LOGGER = logging.getLogger('elampdu')
    
    """CRC16 lookup table."""
    CRC16_TABLE = (
        0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601,
        0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0,
        0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1, 0xCB81,
        0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941,
        0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01,
        0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0,
        0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081,
        0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
        0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00,
        0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0,
        0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981,
        0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41,
        0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700,
        0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0,
        0x2080, 0xE041, 0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281,
        0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
        0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01,
        0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1,
        0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 0x7EC0, 0x7F80,
        0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541,
        0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101,
        0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0,
        0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481,
        0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
        0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, 0x8801,
        0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1,
        0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581,
        0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380, 0x8341,
        0x4100, 0x81C1, 0x8081, 0x4040
    )
    

    @classmethod
    def _calculate_crc16(cls, data: Union[bytes, bytearray], big: bool = False) -> bytes:
        """Calculates the CRC16."""
        crc = 0xFFFF
    
        for char in data:
            crc = (crc >> 8) ^ cls.CRC16_TABLE[((crc) ^ char) & 0xFF]
    
        return struct.pack('>H' if big else '<H', crc)
        
    @classmethod
    def _fmt_data(cls, data: Union[bytes, bytearray]) -> str:
        if len(data) > 32:
            return binascii.hexlify(data[0:33]) + b'...'
        else:
            return binascii.hexlify(data)
    
# generic classes

class ElamException:
    """Represents an ELAM protocol exception."""
    
    def __init__(self, error_code: int, exception_code: int) -> None:
        """Constructor."""
        self.error_code = error_code
        self.exception_code = exception_code

    def to_bytes(self, elam_addr: int) -> bytes:
        """Returns an ELAM Exception ADU for provided station address."""
        ba = bytearray(struct.pack('>HBB', (elam_addr - 248) | (248 << 8), self.error_code, self.exception_code))
        crc_calc = ElamUtil._calculate_crc16(ba)
        ba.append(crc_calc[0])
        ba.append(crc_calc[1])
        ElamUtil.LOGGER.debug('ElamException {}'.format(ba))
        return bytes(ba)

    @classmethod
    def from_bytes(cls, bb: bytes) -> Union[object, None]:
        """Returns an instance of ElamException from bytes or None if any error."""
        crc_rcvd = bb[-2:]
        crc_calc = ElamUtil._calculate_crc16(bb[0:-2])
        ElamUtil.LOGGER.debug('ElamException.from_bytes crc_rcvd {}, crc_calc {}'.format(crc_rcvd, crc_calc))
        if (crc_rcvd[0] != crc_calc[0]) or (crc_rcvd[1] != crc_calc[1]):
            return None
        addr_h, addr_l, error_code, exception_code = struct.unpack_from('>BBBB', bb, 0)
        ElamUtil.LOGGER.debug('error_code = {}, exception_code = {}'.format(error_code, exception_code))
        return ElamException(error_code, exception_code)

    @classmethod
    def from_serial(cls, bb: bytes, ser: object) -> Union[object, None]:
        """Returns an instance of ElamException from bytes and serial 
        source or None if any error."""
        bb += ser.read(3)
        if len(bb) == 6:
            return cls.from_bytes(bb)                
        else:
            ElamUtil.LOGGER.debug('ElamException.from_serial return None len={}'.format(len(bb)))
            return None

    def __repr__(self):
        return 'ElamException({}, {})'.format(self.error_code, self.exception_code)


class ElamRQ:
    """Represents an ELAM protocol request."""
    
    @classmethod
    def from_serial(cls, ser: object) -> Union[object, None]:
        """Returns an instance of ElamRQ from serial source or None 
        if any error."""
        ElamUtil.LOGGER.debug('en ElamRQ ')
        bb = ser.read(3)
        if len(bb) == 3:
            sih, sil, fc = struct.unpack_from('>BBB', bb, 0)
            station_id = (((sih & 0x7) << 8) | sil) + 248
            ElamUtil.LOGGER.debug('en ElamRQ sam_id = {}, fc = {}'.format(station_id, fc))
            if fc < 5:
                return station_id, ElamReadRQ.from_serial(bb, ser)
            else:
                return station_id, ElamWriteRQ.from_serial(bb, ser)
        else:
            return None

class ElamRS:
    """Represents an ELAM protocol response."""
    
    @classmethod
    def from_serial(cls, ser: object) -> Union[object, None]:
        """Returns an instance of ElamRS from serial source or None 
        if any error."""
        bb = ser.read(3)
        if len(bb) == 3:
            sih, sil, fc = struct.unpack_from('>BBB', bb, 0)
            station_id = (((sih & 0x7) << 8) | sil) + 248
            ElamUtil.LOGGER.debug('en ElamRS sam_id = {}, fc = {}'.format(station_id, fc))
            if fc < 5:
                return station_id, ElamReadRS.from_serial(bb, ser)
            elif fc < 0x80:
                return station_id, ElamWriteRS.from_serial(bb, ser)
            else:
                return station_id, ElamException.from_serial(bb, ser)
        else:
            return None

# read operation classes

class ElamReadRQ:
    """Represents an ELAM protocol request for a read function."""
    
    def __init__(self, fc: int, st_address: int, quantity: int) -> None:
        """Constructor."""
        self.fc = fc
        self.st_address = st_address
        self.quantity = quantity
        
    def to_bytes(self, elam_addr: int) -> bytes:
        """Returns an ELAM read function request ADU for provided 
        station address."""
        ba = bytearray(struct.pack('>HBHH', (elam_addr - 248) | (248 << 8), self.fc, self.st_address, self.quantity))
        crc_calc = ElamUtil._calculate_crc16(ba)
        ba.append(crc_calc[0])
        ba.append(crc_calc[1])
        return bytes(ba)

    @classmethod
    def from_bytes(cls, bb: bytes) -> Union[object, None]:
        """Returns an instance of ElamReadRQ from bytes or None if any error."""
        crc_rcvd = bb[-2:]
        crc_calc = ElamUtil._calculate_crc16(bb[0:-2])
        ElamUtil.LOGGER.debug('ElamReadRQ.from_bytes crc_rcvd {}, crc_calc {}'.format(crc_rcvd, crc_calc))
        if (crc_rcvd[0] != crc_calc[0]) or (crc_rcvd[1] != crc_calc[1]):
            return None
        addr_h, addr_l, fc, st_address, quantity = struct.unpack_from('>BBBHH', bb, 0)
        ElamUtil.LOGGER.debug('fc = {}'.format(fc))
        if fc == 1:
            return ElamReadCoilsRQ(st_address, quantity)
        elif fc == 2:
            return ElamReadDiscreteInputsRQ(st_address, quantity)
        elif fc == 3:
            return ElamReadHoldingRegistersRQ(st_address, quantity)
        elif fc == 4:
            return ElamReadInputRegistersRQ(st_address, quantity)
        else:
            return ElamException(fc | 0x80, 1)
        
    @classmethod
    def from_serial(cls, bb: bytes, ser: object) -> Union[object, None]:
        """Returns an instance of ElamReadRQ from bytes and serial 
        source or None if any error."""
        bb += ser.read(6)
        if len(bb) == 9:
            return cls.from_bytes(bb)                
        else:
            return None

class ElamReadRS:
    """Represents an ELAM protocol response for a read function."""
    
    def __init__(self, fc: int, data: Union[bytes, bytearray]) -> None:
        """Constructor."""
        self.fc = fc
        self.data = bytearray(data)

    def to_bytes(self, elam_addr: int) -> bytes:
        """Returns an ELAM read function response ADU for provided 
        station address."""
        if self.fc < 3:
            ba = bytearray(struct.pack('>HBB', (elam_addr - 248) | (248 << 8), self.fc, len(self.data)))
        else:
            ba = bytearray(struct.pack('>HBH', (elam_addr - 248) | (248 << 8), self.fc, len(self.data)))
        ba.extend(self.data)
        crc_calc = ElamUtil._calculate_crc16(ba)
        ba.append(crc_calc[0])
        ba.append(crc_calc[1])
        return bytes(ba)

    @classmethod
    def from_bytes(cls, bb: bytes) -> Union[object, None]:
        """Returns an instance of ElamReadRS from bytes or None if any error."""
        crc_rcvd = bb[-2:]
        crc_calc = ElamUtil._calculate_crc16(bb[0:-2])
        ElamUtil.LOGGER.debug('ElamReadRS.from_bytes crc_rcvd {}, crc_calc {}'.format(crc_rcvd, crc_calc))
        if (crc_rcvd[0] != crc_calc[0]) or (crc_rcvd[1] != crc_calc[1]):
            return None
        addr_h, addr_l, fc = struct.unpack_from('>BBB', bb, 0)
        if fc < 3:
            data = bb[4:-2]
        else:
            data = bb[5:-2]
        ElamUtil.LOGGER.debug('fc = {}'.format(fc))
        if fc == 1:
            return ElamReadCoilsRS(data)
        elif fc == 2:
            return ElamReadDiscreteInputsRS(data)
        elif fc == 3:
            return ElamReadHoldingRegistersRS(data)
        elif fc == 4:
            return ElamReadInputRegistersRS(data)
        else:
            return ElamException(fc | 0x80, 1)
        
    @classmethod
    def from_serial(cls, bb: bytes, ser: object) -> Union[object, None]:
        """Returns an instance of ElamReadRS from bytes and serial 
        source or None if any error."""
        byte_count = -1
        if bb[2] < 3:
            bb += ser.read(1)
            if len(bb) == 4:
                byte_count = bb[3]
                total_count = 4 + byte_count + 2
        else:
            bb += ser.read(2)
            if len(bb) == 5:
                byte_count = struct.unpack_from('>H', bb, 3)[0]
                total_count = 5 + byte_count + 2
        if byte_count < 0:
            return None
        bb += ser.read(byte_count + 2)        
        if len(bb) == total_count:
            return cls.from_bytes(bb)     
        else:
            return None


class ElamReadCoilsRQ(ElamReadRQ):
    """Represents an ELAM protocol request for a read coils function."""

    def __init__(self, st_address: int, quantity:int) -> None:
        """Constructor."""
        super().__init__(1, st_address, quantity)
        
    def __repr__(self):
        return 'ElamReadCoilsRQ({}, {})'.format(self.st_address, self.quantity)


class ElamReadCoilsRS(ElamReadRS):
    """Represents an ELAM protocol response for a read coils function."""
    
    def __init__(self, data: bytearray) -> None:
        """Constructor."""
        super().__init__(1, data)

    def __repr__(self):
        return 'ElamReadCoilsRS({})'.format(ElamUtil._fmt_data(self.data))


class ElamReadDiscreteInputsRQ(ElamReadRQ):
    """Represents an ELAM protocol request for a read discrete inputs function."""

    def __init__(self, st_address: int, quantity: int) -> None:
        """Constructor."""
        super().__init__(2, st_address, quantity)

    def __repr__(self):
        return 'ElamReadDiscreteInputsRQ({}, {})'.format(self.st_address, self.quantity)


class ElamReadDiscreteInputsRS(ElamReadRS):
    """Represents an ELAM protocol response for a read discrete inputs function."""
    
    def __init__(self, data: bytearray) -> None:
        """Constructor."""
        super().__init__(2, data)

    def __repr__(self):
        return 'ElamReadDiscreteInputsRS({})'.format(ElamUtil._fmt_data(self.data))

class ElamReadHoldingRegistersRQ(ElamReadRQ):
    """Represents an ELAM protocol request for a read holding registers function."""

    def __init__(self, st_address: int, quantity: int) -> None:
        """Constructor."""
        super().__init__(3, st_address, quantity)

    def __repr__(self):
        return 'ElamReadHoldingRegistersRQ({}, {})'.format(self.st_address, self.quantity)


class ElamReadHoldingRegistersRS(ElamReadRS):
    """Represents an ELAM protocol response for a read holding registers function."""
    
    def __init__(self, data: bytearray) -> None:
        """Constructor."""
        super().__init__(3, data)
        
    def __repr__(self):
        return 'ElamReadHoldingRegistersRS({})'.format(ElamUtil._fmt_data(self.data))

class ElamReadInputRegistersRQ(ElamReadRQ):
    """Represents an ELAM protocol request for a read holding registers function."""

    def __init__(self, st_address: int, quantity: int) -> None:
        """Constructor."""
        super().__init__(4, st_address, quantity)

    def __repr__(self):
        return 'ElamReadInputRegistersRQ({}, {})'.format(self.st_address, self.quantity)


class ElamReadInputRegistersRS(ElamReadRS):
    """Represents an ELAM protocol response for a read holding registers function."""
    
    def __init__(self, data: bytearray) -> None:
        """Constructor."""
        super().__init__(4, data)

    def __repr__(self):
        return 'ElamReadInputRegistersRS({})'.format(ElamUtil._fmt_data(self.data))

# write operation classes

class ElamWriteRQ:
    """Represents an ELAM protocol request for a write function."""
    
    def __init__(self, fc: int, st_address: int, quantity: int, data: Union[bytes, bytearray]) -> None:
        """Constructor."""
        self.fc = fc
        self.st_address = st_address
        self.quantity = quantity
        self.data = bytearray(data)

    def to_bytes(self, elam_addr: int) -> bytes:
        """Returns an ELAM write function request ADU for provided 
        station address."""
        if self.fc < 15:
            ba = bytearray(struct.pack('>HBH', (elam_addr - 248) | (248 << 8), self.fc, self.st_address))
        else:
            ba = bytearray(struct.pack('>HBHHH', (elam_addr - 248) | (248 << 8), self.fc, self.st_address, self.quantity, len(self.data)))
        ba.extend(self.data)
        crc_calc = ElamUtil._calculate_crc16(ba)
        ba.append(crc_calc[0])
        ba.append(crc_calc[1])
        return bytes(ba)
            
    @classmethod
    def from_bytes(cls, bb: bytes) -> Union[object, None]:
        """Returns an instance of ElamWriteRQ from bytes or None if any error."""
        crc_rcvd = bb[-2:]
        crc_calc = ElamUtil._calculate_crc16(bb[0:-2])
        ElamUtil.LOGGER.debug('ElamWriteRQ.from_bytes crc_rcvd {}, crc_calc {}'.format(crc_rcvd, crc_calc))
        if (crc_rcvd[0] != crc_calc[0]) or (crc_rcvd[1] != crc_calc[1]):
            return None
        addr_h, addr_l, fc = struct.unpack_from('>BBB', bb, 0)
        if fc < 15:
            st_address = struct.unpack_from('>H', bb, 3)[0]
            data = bb[5:-2]
        else:
            st_address, quantity, byte_count = struct.unpack_from('>HHH', bb, 3)
            data = bb[9:-2]
        ElamUtil.LOGGER.debug('fc = {}'.format(fc))
        if fc == 5:
            return ElamWriteCoilRQ(st_address, data)
        elif fc == 6:
            return ElamWriteHoldingRegisterRQ(st_address, data)
        elif fc == 15:
            return ElamWriteCoilsRQ(st_address, quantity, data)
        elif fc == 16:
            return ElamWriteHoldingRegistersRQ(st_address, quantity, data)
        else:
            return ElamException(fc | 0x80, 1)
        
    @classmethod
    def from_serial(cls, bb: bytes, ser: object) -> Union[object, None]:
        """Returns an instance of ElamWriteRQ from bytes and serial 
        source or None if any error."""
        byte_count = -1
        total_count = -1
        if bb[2] < 15:
            bb += ser.read(4)
            ElamUtil.LOGGER.debug('en ElamWriteRQ.from_serial len(bb) = {}'.format(len(bb)))
            if len(bb) == 7:
                byte_count = 0
                total_count = 7 + byte_count + 2
        else:
            bb += ser.read(6)
            if len(bb) == 9:
                byte_count = struct.unpack_from('>H', bb, 7)[0]
                total_count = 9 + byte_count + 2
        ElamUtil.LOGGER.debug('en ElamWriteRQ.from_serial byte_count = {}, total_count = {}'.format(byte_count, total_count))
        if byte_count < 0:
            return None
        bb += ser.read(byte_count + 2)        
        if len(bb) == total_count:
            return cls.from_bytes(bb)     
        else:
            return None


class ElamWriteCoilRQ(ElamWriteRQ):
    """Represents an ELAM protocol request for a write function."""
    
    def __init__(self, st_address: int, data: bytes) -> None:
        """Constructor."""
        super().__init__(5, st_address, 1, data)

    def __repr__(self):
        return 'ElamWriteCoilRQ({}, {})'.format(self.st_address, ElamUtil._fmt_data(self.data))


class ElamWriteHoldingRegisterRQ(ElamWriteRQ):
    """Represents an ELAM protocol request for a write holding register function."""
    
    def __init__(self, st_address: int, data: bytes) -> None:
        """Constructor."""
        super().__init__(6, st_address, 1, data)

    def __repr__(self):
        return 'ElamWriteHoldingRegisterRQ({}, {})'.format(self.st_address, ElamUtil._fmt_data(self.data))

class ElamWriteCoilsRQ(ElamWriteRQ):
    """Represents an ELAM protocol request for a write coils function."""
    
    def __init__(self, st_address: int, quantity: int, data: bytes) -> None:
        """Constructor."""
        super().__init__(15, st_address, quantity, data)

    def __repr__(self):
        return 'ElamWriteCoilsRQ({}, {}, {})'.format(self.st_address, self.quantity, ElamUtil._fmt_data(self.data))

class ElamWriteHoldingRegistersRQ(ElamWriteRQ):
    """Represents an ELAM protocol request for a write holding registers function."""
    
    def __init__(self, st_address: int, quantity: int, data: bytes) -> None:
        """Constructor."""
        super().__init__(16, st_address, quantity, data)

    def __repr__(self):
        return 'ElamWriteHoldingRegistersRQ({}, {}, {})'.format(self.st_address, self.quantity, ElamUtil._fmt_data(self.data))

class ElamWriteRS:
    """Represents an ELAM protocol response for a write function."""
    
    def __init__(self, fc: int, st_address: int, value: Union[int, bytes, bytearray]) -> None:
        """Constructor."""
        self.fc = fc
        self.st_address = st_address
        self.value = value
        
    def to_bytes(self, elam_addr: int) -> bytes:
        """Returns an ELAM write function response ADU for provided 
        station address."""
        ElamUtil.LOGGER.debug('>>>>>>>>>>>>>> {}'.format(type(self.value)))
        if isinstance(self.value, bytes) or isinstance(self.value, bytearray):
            ba = bytearray(struct.pack('>HBH', (elam_addr - 248) | (248 << 8), self.fc, self.st_address))
            ba.extend(self.value)
        else:
            ba = bytearray(struct.pack('>HBHH', (elam_addr - 248) | (248 << 8), self.fc, self.st_address, self.value))
        crc_calc = ElamUtil._calculate_crc16(ba)
        ba.append(crc_calc[0])
        ba.append(crc_calc[1])
        return bytes(ba)
            
    @classmethod
    def from_bytes(cls, bb: bytes) -> Union[object, None]:
        """Returns an instance of ElamWriteRS from bytes or None if any error."""
        crc_rcvd = bb[-2:]
        crc_calc = ElamUtil._calculate_crc16(bb[0:-2])
        ElamUtil.LOGGER.debug('ElamWriteRS.from_bytes crc_rcvd {}, crc_calc {}'.format(crc_rcvd, crc_calc))
        if (crc_rcvd[0] != crc_calc[0]) or (crc_rcvd[1] != crc_calc[1]):
            return None
        addr_h, addr_l, fc, st_address, value = struct.unpack_from('>BBBHH', bb, 0)
        ElamUtil.LOGGER.debug('fc = {}'.format(fc))
        if fc == 5:
            return ElamWriteCoilRS(st_address, bb[-4:-2])
        elif fc == 6:
            return ElamWriteHoldingRegisterRS(st_address, bb[-4:-2])
        elif fc == 15:
            return ElamWriteCoilsRS(st_address, value)
        elif fc == 16:
            return ElamWriteHoldingRegistersRS(st_address, value)
        else:
            return ElamException(fc | 0x80, 1)
        
    @classmethod
    def from_serial(cls, bb: bytes, ser: object) -> Union[object, None]:
        """Returns an instance of ElamWriteRS from bytes and serial 
        source or None if any error."""
        bb += ser.read(6)
        if len(bb) == 9:
            return cls.from_bytes(bb)
        else:
            return None


class ElamWriteCoilRS(ElamWriteRS):
    """Represents an ELAM protocol response for a write coil function."""
    
    def __init__(self, st_address: int, value: int) -> None:
        """Constructor."""
        super().__init__(5, st_address, value)

    def __repr__(self):
        return 'ElamWriteCoilRS({}, {})'.format(self.st_address, ElamUtil._fmt_data(self.value))


class ElamWriteHoldingRegisterRS(ElamWriteRS):
    """Represents an ELAM protocol response for a write holding register function."""
    
    def __init__(self, st_address: int, value: int) -> None:
        """Constructor."""
        super().__init__(6, st_address, value)

    def __repr__(self):
        return 'ElamWriteHoldingRegisterRS({}, {})'.format(self.st_address, ElamUtil._fmt_data(self.value))

class ElamWriteCoilsRS(ElamWriteRS):
    """Represents an ELAM protocol response for a write coils function."""
    
    def __init__(self, st_address: int, value: Union[bytes, bytearray]) -> None:
        """Constructor."""
        super().__init__(15, st_address, value)

    def __repr__(self):
        return 'ElamWriteCoilsRS({}, {})'.format(self.st_address, self.value)

class ElamWriteHoldingRegistersRS(ElamWriteRS):
    """Represents an ELAM protocol response for a write holding registers function."""
    
    def __init__(self, st_address: int, value: Union[bytes, bytearray]) -> None:
        super().__init__(16, st_address, value)

    def __repr__(self):
        return 'ElamWriteHoldingRegistersRS({}, {})'.format(self.st_address, self.value)

